#define MAX_NUMBER_OF_OBJECTS 20
#define POOL_SIZE 7 //The number of genetic sequences that we work on

#include <iostream>
#include <fstream>
#include <string>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
using namespace std;

int reproduce(int N, int C, int *s, int *v, int **seq);
double fitness(int N, int C, int *seq);
void crossOverAndMutation(int **sel, int N, int s[], int v[], int C);

    
int main(void){
    clock_t t1,t2; // For timing
    t1=clock();
    //////////////////////////////////////////////////////////
    int N; // The number of objects
    int C; // The capacity of the container
    int s[MAX_NUMBER_OF_OBJECTS]; // The sizes of the objects
    int v[MAX_NUMBER_OF_OBJECTS]; // The values of the objects
    int *seq[POOL_SIZE];

    ofstream output;
    output.open("input.in");
    output << "4 16\n1 5 8\n2 1 1\n3 2 3\n4 3 4";
    output.close();
    
    string line;
    ifstream input;
    input.open("input.in");
  
    input >> N;
    input >> C;

    s[0] = 0;
    v[0] = 0;
    int i;
    for(input>>i; input.good();input >> i){
          input >> s[i];
          input >> v[i];
    }
    
    srand(time(0));
    for(int j = 0; j<POOL_SIZE ; j++){
          seq[j] = new int[N+2];
          seq[j][N+1] = 0;
          for(int k = 0; k <= N ; k++){
                  //cout << s[k]<<((s[k]==0)?s[k]:((int)(C/s[k])))<<endl;
                  seq[j][k] = rand()%((int)((s[k]==0)?s[k]:((int)(C/s[k])))+1);
                  seq[j][0] += seq[j][k]; // The aggregated size of the sequence
                  seq[j][N+1] += v[k] * seq[j][k]; // The aggregated value of the sequence
                  cout << seq[j][k]<<" ";
          }
          cout <<"   "<<seq[j][0] <<"  "<<seq[j][N+1]<< endl;
    }
    
    for(int l=0; l < 5; l++){
    reproduce(N, C, s, v,seq);
    }
    cout <<"\n"<<endl;
    for(int i=0; i<POOL_SIZE; i++){
            for(int j=1; j<=N; j++)
                    cout << seq[i][j] <<" ";
            cout <<"  " << seq[i][0] << "  "<<seq[i][N+1] << endl;;
    }
    
    for(int l=0; l<POOL_SIZE; l++)
            delete seq[l];
    cout << "program over"<<endl;
    ///////////////////////////////////////////////
    t2=clock();
    double diff ((double)t2-(double)t1);
    cout<<"\n"<<diff/CLOCKS_PER_SEC << "s"<<endl;
    ///////////////////////////////////////////////
    cout <<"\nPress any key to continue...";
    getchar();
    return 0;
}

int reproduce(int N, int C, int *s, int *v, int **seq)
{
    double f[POOL_SIZE]; // The fitnesses of sequences
    double p[POOL_SIZE]; // The probability of each sequence
    int *sel[POOL_SIZE]; // The chromosomes that survive through natural selection
    double myRand[POOL_SIZE]; // Random numbers between 0 and 1
    double fSum = 0;
    
    for(int i = 0; i < POOL_SIZE; i++){
            f[i] = fitness(N, C, seq[i]);
            fSum += f[i];
    }

    srand(time(0));
    myRand[0] = (double)rand()/RAND_MAX;   
    p[0] = (double)f[0] / fSum;
    for(int i = 1; i < POOL_SIZE; i++){
            p[i] = (double)f[i] / fSum + p[i-1];
            myRand[i] = (double)rand()/RAND_MAX;               
    }
    
    for(int i = 0; i < POOL_SIZE; i++){
            double temp = 0;
            double R = myRand[i];
            for(int j = 0; j < POOL_SIZE; j++){
                    if(R > temp && R < p[j]){
                         sel[i] = seq[j];
                         break;
                    }
                    temp = p[j];
            }
    }
    
    for(int i=0; i<POOL_SIZE; i++)
            for(int j=0; j<=N+1; j++)
                    seq[i][j] = sel[i][j];
    
    crossOverAndMutation(seq, N, s, v, C);
    return 0;
}

double fitness(int N, int C, int *seq)
{
       if(seq[0] == 0) return 0;
       else if(seq[0] > C) return 0;
       else return seq[N+1];    
}

void crossOverAndMutation(int **sel, int N, int *s, int *v, int C){
     srand(time(0));
     for(int i=0; i<POOL_SIZE - 1 ; i++)
     {
        int j=i+1;
        int r = rand()%N + 1;
        int *temp = new int[r];
        for(int k = 1; k <= r ; k++){
                temp[k] = sel[i][k];
                sel[i][0] -= sel[i][k];
                sel[i][N+1] -= sel[i][k] * v[k];
                
                double pMutation = (double)rand()/RAND_MAX;
                if(pMutation < 1./100) sel[i][k] = rand()%(int)(C/s[k] + 1);
                else sel[i][k] = sel[j][k];
                
                sel[i][0] += sel[i][k];
                sel[i][N+1] += sel[i][k] * v[k];
        
                sel[j][0] -= sel[j][k];
                sel[j][N+1] -= sel[j][k] * v[k];
                
                pMutation = (double)rand()/RAND_MAX;
                if(pMutation < 1./100) sel[j][k] = rand()%(int)(C/s[k] + 1);
                else sel[j][k] = temp[k];
                
                sel[j][0] += sel[j][k];
                sel[j][N+1] += sel[j][k] * v[k];
        }
     }     
}
