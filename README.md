This code solves the knapsack problem using a genetic algorithm.

The problem states that we have a series of objects of various sizes and we want to fit as many of them as possible into a knapsack